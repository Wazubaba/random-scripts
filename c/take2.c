#include <stdio.h>

void menu(char name[]){
	printf("  <menu id=\"%s\" label=\"%s\">\n",name,name);
	return;
}

void node(char name[],char cmd[]){
	printf("    <item label=\"%s\">\n",name);
	printf("      <action name=\"Execute\"><execute>%s</execute></action>\n",cmd);
	printf("    </item>\n");
	return;
}

void sep(char name[]=""){
	if(name != ""){
		printf("    <separator id=\"%s\"/>\n");
	}
	else{
		printf("    <separator />\n");
	}
	return;
}

void emenu(){
	printf("  </menu>\n");
	return;
}

int main(int argc, char *argv[]){
//	char name[] = "testmenu"; This will fix the warnings, but is it really worth doing this for every damn thing?
	menu("2D");
	node("aseprite","aseprite");
	emenu();
	
	menu("text");
	node("Sublime Text 2","~/tools/sublimetext2/sublime_text");
	node("Geany","geany");
	emenu();

	menu("games");
	node("Steam", "steam");
	emenu();
	return 0;
}