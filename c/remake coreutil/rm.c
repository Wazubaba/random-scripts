#include <stdio.h>

//extremely simple version of the linux rm command

int main(int argc, char *argv[]){
	//handle args

	if (argc < 2){
		printf("[+]Usage: rm file(s)\n");
		return 2;
	}

	int i;
	for( i = 1; i < argc; i = i + 1 ){
		if((remove(argv[i])) != 0){
			printf("[+]Warn: unable to delete %s\n",argv[i]);
		}
	}
	return 0;
}
