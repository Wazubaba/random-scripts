#include <stdio.h>
#include <string.h>

void write(char lib[]);
static char * read(char lib[]);

int main(void){
	printf("works\n");
	//write("testfile");
	printf("%s\n",read("configtest.c"));
	return 0;
}

void write(char lib[]){
	printf("[+]Writing: %s\n",lib);
	FILE *file;
	file = fopen(lib,"w");
	fprintf(file, "data\n");
	fclose(file);
	return;
}

static char * read(char lib[]){
	printf("[+]Reading: %s\n",lib);
	FILE *file;
	file = fopen(lib,"r");
	static char line[55];
	int i = 0;
	fgets(line,55,file);
	return line;
}
