#include <stdio.h>

//extremely simple version of the linux cp command

int main(int argc, char *argv[]){

	//handle args
	if(argc<3){
		printf("USAGE: cp inputfile outputfile\n");
		return 2;
	}
	FILE *i,*o;
	i = fopen(argv[1],"r");
	o = fopen(argv[2],"w");

	int c;
	while((c=getc(i)) != EOF){
		fprintf(o,"%c",c);
	}

	//cleanup
	fclose(i);
	fclose(o);
	return 0;
}
