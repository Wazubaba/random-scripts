#include <stdio.h>

/*
+========================================================+
|attempt to finally learn and fully comprehend C file I/O|
+========================================================+
*/

char read()
{
	char buffer[11]; 
	FILE *targ;
	if(targ = fopen("testfile","r"))
	{
		fread(buffer,1,10,targ);
		buffer[10] = 0;
		fclose(targ);
		printf("%s\n",buffer);
	}
	return 0;
}


int main(void)
{
	printf("test\n");
	//read();

	FILE *test;
	test = fopen("testfile","r");
	int c;
	while((c = getc(test)) != EOF)
	{
		printf("%c",c);
	}

	fclose(test);

	return 0;
}