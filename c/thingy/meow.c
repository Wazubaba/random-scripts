#include <stdio.h>

struct meow {
	int x;
	int y;
	char *nya;
};

struct meow test(){
	struct meow blah;
	blah.nya = "oi!\n";
	return blah;
}

int main(int argv, char * argc[]){

	struct meow rawr;
	rawr.x = 5;
	rawr.y = 2;
	rawr.nya = "nyanya~\n";

	printf("rawr.y=%d\n",rawr.y);
	printf("%s",rawr.nya);

	printf("meow\n");
	if(argv > 1){
		printf("%s\n",argc[1]);
	}

	printf("now to get freaky...\n");
	struct meow newnya;
	newnya = test();
	printf("%s\n",newnya.nya);
	return 0;
}