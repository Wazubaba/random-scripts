#include <stdio.h>

int main(int argc, char *argv[]){
	int i;
	if(argc<2){
		printf("need at least one arg.\n");
		return 2;
	}
	for(i=0;i<argc;i++){
		printf("%s\n",argv[i]);
	}
	return 0;
}