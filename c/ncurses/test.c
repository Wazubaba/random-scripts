#include <stdio.h>
#include <ncurses.h>
#include <string.h>

void centerprint(char line[]);

void testinput();

int main(){

	//init display
	initscr();
	raw();
	keypad(stdscr, TRUE);
	noecho();

	int ch; //our char variable

	printw("Type any character to see it in bold\n");
	ch = getch();

	if(ch == KEY_F(1)){
		printw("F1 Key pressed\n");
		centerprint("meow");
	}
	else if(ch == KEY_F(2)){
		testinput();
	}

	else{
		printw("The pressed key is ");
		attron(A_BOLD);
		printw("%c", ch);
		attroff(A_BOLD);
		printw(".\n");
	}

	//deinitialize screen
	refresh();
	getch();
	endwin();

	//end
	return 0;
}


void centerprint(char line[]){
	int row,col;
	getmaxyx(stdscr,row,col);
	mvprintw(row/2,(col-strlen(line))/2, "%s",line);
	return;
}

void testinput(){
	char mesg[]="Enter a string: ";
	char str[80];
	int row,col;
	echo();
	centerprint(mesg);

	getstr(str);
	mvprintw(LINES - 2, 0, "You Entered: %s", str);
	noecho();
	return;
}