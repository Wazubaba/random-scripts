class conf():
	def __init__(self):
		pass

	def findtitlekey(self,title,key):
		for line in range(0, len(self.dat)):
			if self.dat[line] == '['+title+']':
				start = line
				for line in range(start+1,len(self.dat)):
					if '[' and ']' in self.dat[line]:
						end = line -1
						break
				for line in range(start+1,end):
					if self.dat[line] == key:
						return start+line
			else: raise KeyError

	def write(self,file):
		temp = open(file,'w')
		for line in self.dat:
			temp.write(line+'\n')
		temp.close()

	def read(self,file):
		temp = open(file,'r')
		self.dat = temp.readlines()
		temp.close()
		del temp
		return

	def get(self,title,key):
		indx = self.findtitlekey(title,key)
		return self.dat[indx].split('=')[1]

	def set(self,title,key,val):
		indx = self.findtitlekey(title,key)
		tempstore = self.dat[indx]
		temp = tempstore.split('=')[0]
		temp += '='+str(val)
		self.dat[indx] = temp
		del temp, tempstore

#this sadly does not work right yet...
	def readdict(self,file):
		temp = open(file,'r')
		tl = temp.readlines()
		temp.close
		del temp
		temp = {}
		self.dat = {}
		for line in range(0,len(tl)):
			if '[' or ']' in tl[line]:
				title = tl[line].strip('[').strip(']')
				start = line
				for line in range(start+1,len(tl)):
					if '[' and ']' in tl[line]:
						end = line-1
						break
				for line in range(start+1,end):
					if tl[line] == '': continue
					newkeyval = tl[line].split('=')
					temp[newkeyval[0].strip('\r\n')] = newkeyval[1].strip('\r\n')
				self.dat[title] = temp

test = conf()
test.read('test')
print test.get('rawr','meow')

