#!/usr/bin/python
import os
class imglist():
	def __init__(self):
		self.imgform = [
                '.png',
                '.bmp', 
                '.jpg',
                '.jpeg',
                '.tga',
                '.gif',
                ] 
		self.imglist = []
		self.imgdir = ''

	def clean(self):
		temp = []
		for file in self.imglist:
			if file.find(" "): #we found a space
				charlist = [] #we need to turn the filename into a big list of chars for easy alteration
				for char in file: charlist.append(char)
				
				for char in range(0,len(charlist)): #iterate over each letter in our list
					if charlist[char] == " ":charlist[char] = "\ " #escape our spaces
					if charlist[char] == "(":charlist[char] = "\("
					if charlist[char] == ")":charlist[char] = "\)"
					if charlist[char] == "[":charlist[char] = "\["
					if charlist[char] == "]":charlist[char] = "\]"

				rebuiltstring = ""
				for char in charlist: #rebuild our string
					rebuiltstring += char

			else: temp.append(file) #if we don't find a space, go ahead and add the file right on in

			#and who says strings are immutable ^_^
			temp.append(rebuiltstring)
		self.imglist = temp
					
			

	def get(self,path):
		self.imgdir = path
		self.imgs = os.listdir(path)
		for img in self.imgs:
			for form in self.imgform:
				if form in img:
					self.imglist.append(img)
					break
		self.clean()

	def write(self,file):
		temp = open(file,'w')
		temp.write(self.imgdir+'\n')
		for line in self.imglist:
			temp.write(line+'\n')
		temp.close()

	def read(self,file):
		temp = open(file,'r')
		tl = temp.readlines()
		temp.close()
		self.imgdir = tl[0].strip('\r\n')
		for line in range(1,len(tl)):
			if tl[line] != '' or '\n' or ' ' or '\r\n':
				self.imglist.append(tl[line].strip('\r\n'))
