import os,sys,time
from distutils import dir_util

interval= 25 #set this to the interval of backups you want, in seconds
#if you want to specify minutes, just do the amount of minutes you want * 60; example: interval= 25 * 60 would be 25 minutes

curdir = os.getcwd()

print "[+]INFO: Scrumble SaveGuard started."

while 1:
	time.sleep(interval)
	print "[+]INFO: Beginning archive, please don't shutdown scrumbleship!"
	dir_util.copy_tree(src=curdir+"/saves", dst=curdir+"/savebkup", verbose=1)


