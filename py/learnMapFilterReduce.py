import functools

#filter example
rawr = [34,56,435,4,5,35,34,56,46,42,6,35,23,5]
rawr = filter((lambda x: x < 10),rawr)
for r in rawr: print r

#reduce example
rawr = ["This ", "is ", "a ", "sentence."]
print functools.reduce((lambda x,y: x+y), rawr)

#map example

rawr = [2,4,6,8,10]

rawr = map((lambda x: x/2),rawr)
for r in rawr: print r
