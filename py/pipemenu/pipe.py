import os, sys

def gen(t,nm="",cmd=""):
	if t == "menu":print "<menu id=\""+nm+"\" label=\""+nm+"\">"
	if t == "sep":print "<separator />"
	if t == "node":print "    <item label=\""+nm+"\">\n        <action name=\"Execute\"><execute>"+cmd+"</execute></action>\n    </item>"
	if t == "emenu":print "</menu>"

gen("menu","test")
gen("node","itemtest","RAWRMOD")
gen("sep")
gen("emenu")