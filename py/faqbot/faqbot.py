import threading
import socket
import sys
import time
import hashlib
import Queue as queue

#these are my core settings, configure as you wish:

name = "faqbot"
#defaultserv = "timeofeve.irc.so"
#defaultchan = "#timeofeve"
defaultserv = "irc.wazuclan.com"
defaultchan = "#minetest"
defaultport = 6667

#==================================================#

#this tests for arguments
if len(sys.argv)>1:# we always get 1 arg: the executed file
	try: chan=sys.argv[1]
	except:pass
	try: serv=sys.argv[2]
	except:pass
	try: port=int(sys.argv[3]) # we want port to be a number
	except:pass


class ircbot():
	def __init__(self,info={}):
		#deconstruct the table of data
		self.name = info["name"]
		self.serv = info["serv"]
		self.chan = info["chan"]
		self.port = info["port"]
		self.active = True
		self.data = ""
		self.lock = threading.Lock()
		self.dehooker = False
		self.connection = False

		self.msg = ""
		self.chn = ""
		self.usr = ""
		self.hst = ""

		self.kicktog = False

		self.testtimer1val = 0
		self.testtimer1status = False

		self.HELP={
			"english":"Help test",
			"meow":"nyanyan gozaimasu ^(^-^)",
		}



		self.connect()



		#start data grab thread
		threading.Thread(target = self.timerfunctions).start()

		#start main loop, where we handle cmds
		self.main()

	def connect(self):
		self.sock = socket.socket()
#		self.sock.settimeout(5)  # may be needed sometimes, idk
		self.sock.connect((self.serv,self.port))
		print self.name,"->Connecting to",self.serv,self.port
		self.sock.send('IDENT '+self.name+'\r\nNICK '+self.name+'\r\nUSER '+self.name+' '+self.name+' '+self.name+' '+' :'+self.name+'\r\n')
#		time.sleep(3)  # sometimes needed
		print self.name,"->Joining",self.chan
		self.sock.send('JOIN '+self.chan+'\r\n')
		print self.sock.recv(128)
		self.connection = True

	def main(self):
		#this is our main function, where we handle communication with the server
		while self.active:
			self.data = self.sock.recv(256)
			if self.data == "": self.connection = False
			while self.connection == False:
				try: self.connect()
				except: time.sleep(10) #wait for 10 seconds before trying to reconnect
			if self.data.find('KICK') != -1:
				print self.name,"->Rejoining",self.chan
				self.sock.send('JOIN '+self.chan+'\r\n') 
				if self.kicktog:
					self.sock.send('PRIVMSG '+self.chan+' :Dont kick me! >_<\r\n')
				self.kicktog = True
			if self.data.find('PING') != -1: self.sock.send('PONG '+self.data.split()[1]+'\r\n')

			#parse the data, then do stuff based off it
			self.parse()
			self.handlers()



	def handlers(self):
		#This is where we check for commands and do things based off them
		if self.msg.find("!shutdown") != -1:
			print self.msg
		if self.msg.split()[0] == "!shutdown":
			try:
				if self.msg.split()[1] == self.name:
					print "[L]"+self.usr+" issued shutdown."
					if self.usr.lower() == "wazubaba":
						self.active = False
					else:
						self.sock.send("PRIVMSG "+self.chan+" :Sorry, only Wazubaba can run that command :P\r\n")
			except: pass

		if self.msg.split()[0] == "!help":
			if len(self.msg.split()) > 1:
				lang = self.msg.split()[1].strip("\r\n")
			else: lang = "english"
			try:
				helpmsg = self.HELP[lang]
				self.sock.send("PRIVMSG "+self.chan+" :"+helpmsg+"\r\n")	
			except:
				self.sock.send("PRIVMSG "+self.chan+" :I'm sorry, I don't have help files in that language or the command was not understood, please ask an admin for help :\r\n")
			




		if self.dehooker == False: #if this turns false, fun happens
			#flush data so we dont repeat aka spam :D
			self.msg = ""
			self.chn = ""
			self.usr = ""
			self.hst = ""
		if self.dehooker == True:
			irc.send("PRIVMSG "+self.chan+" :SPAMTEST\r\n")


	def timerfunctions(self):
		#this is where anything that needs to work ouside the main loop goes.
		while self.active:
			if self.testtimer1status == True:
				self.testtimer1val += 1
			time.sleep(1)


	def parse(self):
		# this ugly bit parses the data for the handler function
		if self.data.find('PRIVMSG'):
			try: self.msg = self.data.split(':',2)[2].strip('\r\n')
			except: self.msg = 'nullmsg'
			try: self.usr = self.data.split('!')[0][1:]
			except: self.usr = 'nullusr'
			try: self.chn = self.data.split()[2]
			except: self.chn = 'nullchn'
			try: self.hst = self.data.split()[0].split('!')[1].split('@')[1]
			except: self.hst = 'nullhst'
			self.data = ""
			if self.chn != 'nullchn' and self.msg != 'nullmsg': print '<'+self.usr+'@'+self.hst+'>'+self.msg
			if self.msg == "": self.msg = "nullmsg"
			if self.usr == "minetest44":
				#print self.msg.split(">")[0].strip("<")
				#try: print self.msg.split(">")[1][1:]
				#except: pass
				try:
					self.usr = self.msg.split(">")[0].strip("<")
					self.msg = self.msg.split(">")[1][1:]
				except: pass




#===========================================#
#these are basic setup bits and constructers#
#===========================================#
#if we haven't specified these than apply
#default settings:
if 'serv' not in vars(): serv = defaultserv
if 'chan' not in vars(): chan = defaultchan
if 'port' not in vars(): port = defaultport

#construct our info table
settings = {
	"name" : name,
	"serv" : serv,
	"chan" : chan,
	"port" : port
}

#now we finally start the botclass
I = ircbot(settings)
