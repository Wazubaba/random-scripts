+======================================+
				SaveGuard
	  "A utility to guard dem saves"
	   			~Wazubaba
+======================================+
	This is an extremely simple script
I made after hearing how GeneralVonDoom
kept losing ships he/she was working on.

What's sad is that the image above took
longer to make than this script. XD

To run this script, you will need python
installed. If you are on windows, you
can get python free at

http://python.org/download/

If you are on linux or mac, odds are good
you already have python installed.

This script uses version 2.7.3, and will
not work with python 3 (unless you wanna
port it yourself :P).

+======================================+
				 LICENSE
+======================================+

	Boring legal part incoming... (-_-)

I take no responsibility for damages of
any form this script may or may not
cause, nor does Orange Hat Tech. This
script is not made nor endorsed by
Orange Hat Tech.

This script is the intellectual property
of Wazubaba, released open-source in the
hopes that the community or people that
use it can benefit.

There are no warrenties of any kind that
come with this script.

If you aren't sure, you can check the
source code of the script and see that
it utilizes a standard python library
to perform all file IO, so it ought to
be perfectly safe.

+======================================+
			  HOW TO INSTALL
+======================================+

	Installation of the script is simple,
throw it into scrumbleship's main folder.
As a note, if you can see the saves folder,
you are in the right place!

+======================================+
			 HOW TO CONFIGURE
+======================================+

	If you want to alter the interval at
which the script backs up your saves,
edit the line indicated in the file.
You can open any .py script with any
text editor, though stay away from
MS word, or anything like it for
editing it ;)

+======================================+
			   HOW TO RUN
+======================================+

	To run the script, all you need to do
is open a command prompt or terminal,
navigate to where it is (hopefully in your
scrumbleship folder...), and type:
"python saveguard.py". If all goes well,
you should see the line:
"[+]INFO: Scrumble SaveGuard started."

If you get an error about python not being
a valid command, then that means python is
not installed properly.

+======================================+
			   FINAL NOTES
+======================================+

	To the best of my knowlege, this
script should be bug-free. Just run it
before you fire up scrumble, and it
should be able to work its magic ;)


+======================================+
		 POSSIBLE FUTURE UPDATES
+======================================+

	Update-wise, I may add a bit of
redundency, and have it use 3 backups
that it alternates between. I *might*
also add a GUI, though I am not very
good at doing GUI code in python :(

	This of course all depends on how
useful this tool proves to be :)

	I hope this helps you guys out
while scrumble is having its growing
pains!

	~Wazubaba