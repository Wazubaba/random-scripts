#!/usr/bin/env python

import pygtk
pygtk.require("2.0")
import gtk

class gui:
	def __init__(self):
		self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
		self.window.connect("delete_event", self.del_ev)
		self.window.connect("destroy", self.destroy)
		self.window.set_border_width(10)
		self.button = gtk.Button("no")
		self.button.connect("clicked",self.hello, None)
		self.window.add(self.button)
		self.button.show()
		self.window.show()

	def hello(self, widget, event, data=None):
		print "no"

	def del_ev(self, widget, event, data=None):
		print "del event occured..."
		return False

	def destroy(self, widget, data=None):
		gtk.main_quit()

	def main(self):
		gtk.main()

if __name__ == "__main__":
	infa = gui()
	infa.main()