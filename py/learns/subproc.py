#!/usr/bin/python
import subprocess

def run(cmd):
	p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
	while 1:
		ret = p.poll()
		line = p.stdout.readlines()
		return line

out = run(["screen", "-x"])
for line in out[1:]:
	if line.find("."):
		try: print line.strip('\r\n').split("\t")[1]
		except: pass
