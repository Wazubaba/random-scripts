import sys

if len(sys.argv) < 2:
	print "Usage: "+sys.argv[0]+" code"
	sys.exit()

try:
	p = open("./key","r").read().split("\r\n")[0]
except IOError:
	print "Please generate a text password in this directory called key"
	sys.exit()
if sys.argv[1] == p:
	print "Password accepted, welcome back!"
	print p+" == "+sys.argv[1]+" :D"
sys.exit()