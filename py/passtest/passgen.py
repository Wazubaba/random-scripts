import random,sys


if len(sys.argv) < 2:
	print "Usage: "+sys.argv[0]+" length file"
	sys.exit()

random.seed()

lowchars = ['a','b','c','d','e','f','g','h','i','j','k','l','m','o','p','q','r','s','t','u','v','w','x','y','z']
upchars = ['A','B','C','D','E','F','G','H','I','J','K','L','M','O','P','Q','R','S','T','U','V','W','X','Y','Z']
nums = ['1','2','3','4','5','6','7','8','9','0']

p = ""

for char in range(0,int(sys.argv[1])):
	meow = random.choice([lowchars,upchars,nums])
	p += random.choice(meow)

out = open(sys.argv[2],"w")
out.write(p)
out.close()

print "password generated succesfully."
sys.exit()
