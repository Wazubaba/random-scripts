class config():
	def __init__(self,lib="./.dimcha.conf"):
		self.lib = lib
		self.load()

	def save(self):
		temp = open(self.lib,"w")
		for key in self.data:
			temp.write(key+"|"+self.data[key])
		temp.close()

	def load(self):
		try:
			temp = open(self.lib,"r").readlines()
		except IOError:
			print "Dimcha: Unable to load config, attempting to generate a new one..."
			try:
				temp = open(self.lib,"w")
				temp.close()
			except IOError:
				print "Dimcha: Can't write config :("
				quit()
		for line in temp:
			key,val = line.split("|")
			self.data[key]=val

	def reload(self):
		self.save()
		self.load()

	def setval(self,key,val):
		self.data[key]=val


class imglist():
	def __init__(self,lib=""):
		self.lib = lib
		self.load()
		self.imgexts = [
			"png",
			"bmp",
			"jpg",
			"gif",
			"jpeg",
			"tga",
		]

	def load(self):
		try:
			temp = open(self.lib,"r").readlines()
		except IOError:
			print "Dimcha: Unable to load image list :("
			quit()
		self.path = temp[0]
		self.imgs = []
		for line in range(1,len(temp)):
			self.imgs.append(line)

	def save(self,targ,name):
		temp = open("./"+name)
		temp.write(targ+"\n")
		for file in os.listdir(targ):
			if file.split(".")[1] in self.imgexts:
				temp.write(file+"\n")
		temp.close()

