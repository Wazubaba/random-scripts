import random

def comp():
	lib = open("lib","r")
	chain = {}
	w1 = '\n'
	w2 = '\n'

	for line in lib:
		for curword in line.split():
			if curword != "":
				chain.setdefault((w1, w2), []).append(curword)
				w1 = w2
				w2 = curword
	return chain

def make(chain,maxw = 25):
	try:
		temp = ""
		random.seed()
		wordset = random.choice(chain.keys())
		w1 = wordset[0]
		w2 = wordset[1]
		for i in xrange(random.randrange(1,maxw)):
			new = random.choice(chain[(w1, w2)])
			temp = temp + " " + new
			w1 = w2
			w2 = new
		return temp
	except:
		return 0

cmd = ""
chain = comp()

while cmd != "quit":
	cmd = raw_input(">")
	print make(chain)