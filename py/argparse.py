def parseargs(arglist=[],flags=[],standalone=[],special=[]):
	workspace = ""
	settings = {}

	for argnum in range(1,len(arglist)):
		if arglist[argnum] in flags:
			if "-" not in arglist[argnum+1]:
				settings[arglist[argnum]] = arglist[argnum+1]

	for arg in arglist:
		if arg in standalone:
			settings[arg] = "TRUE"

	for argnum in range(1,len(arglist)):
		if arglist[argnum] in special:
			if "-" not in arglist[argnum+1]:
				if "-" not in arglist[argnum+2]:
					settings[arglist[argnum]] = (arglist[argnum+1],arglist[argnum+2])
	return settings