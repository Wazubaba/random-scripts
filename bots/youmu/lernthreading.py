import threading
import socket
import sys
import time
import hashlib
import Queue as queue

#these are my core settings, configure as you wish:

name = "Youmu"
#defaultserv = "timeofeve.irc.so"
#defaultchan = "#timeofeve"
defaultserv = "irc.wazuclan.com"
defaultchan = "#wazuclan"
defaultport = 6667

#==================================================#

#this tests for arguments
if len(sys.argv)>1:# we always get 1 arg: the executed file
	try: chan=sys.argv[1]
	except:pass
	try: serv=sys.argv[2]
	except:pass
	try: port=int(sys.argv[3]) # we want port to be a number
	except:pass


class ircbot():
	def __init__(self,info={}):
		#deconstruct the table of data
		self.name = info["name"]
		self.serv = info["serv"]
		self.chan = info["chan"]
		self.port = info["port"]
		self.active = True
		self.data = ""
		self.lock = threading.Lock()
		self.dehooker = False

		self.msg = ""
		self.chn = ""
		self.usr = ""
		self.hst = ""
		self.timeractive1=False
		self.timer1val=0
		self.connect()



		#start data grab thread
		#threading.Thread(target = self.datagrab).start()
		threading.Thread(target = self.main).start()


		#start main loop, where we handle cmds
		self.datagrab()

	def connect(self):
		self.sock = socket.socket()
#		self.sock.settimeout(5)
		self.sock.connect((self.serv,self.port))
		print self.name,"->Connecting to",self.serv,self.port
		self.sock.send('IDENT '+self.name+'\r\nNICK '+self.name+'\r\nUSER '+self.name+' '+self.name+' '+self.name+' '+' :'+self.name+'\r\n')
		time.sleep(3)
		print self.name,"->Joining",self.chan
		self.sock.send('JOIN '+self.chan+'\r\n')
		print self.sock.recv(128)

	def datagrab(self):
		#this be the actual threaded function
		#we grab all the data and send it to the parser
		while self.active:
				self.data = self.sock.recv(256)
				if self.data == "": self.connect()
				if self.data.find('KICK') != -1:
					print self.name,"->Rejoining",self.chan
					self.sock.send('JOIN '+self.chan+'\r\n') 
#					self.sock.send('PRIVMSG '+self.chan+' :Dont kick me! >_<\r\n')
				if self.data.find('PING') != -1: self.sock.send('PONG '+self.data.split()[1]+'\r\n')
				self.handlers()

	def encoder(self,line,t):
		if t == "md5": m = hashlib.md5()
		elif t == "sha1": m = hashlib.sha1()
		elif t == "sha224": m = hashlib.sha224()
		elif t == "sha256": m = hashlib.sha256()
		elif t == "sha384": m = hashlib.sha384()
		elif t == "sha512": m = hashlib.sha512()
		m.update(line)
		gen = m.hexdigest()
		return gen

	def parse(self):
		# this ugly bit parses the data for later use
		if self.data.find('PRIVMSG'):
			try: self.msg = self.data.split(':',2)[2].strip('\r\n')
			except: self.msg = 'nullmsg'
			try: self.usr = self.data.split('!')[0][1:]
			except: self.usr = 'nullusr'
			try: self.chn = self.data.split()[2]
			except: self.chn = 'nullchn'
			try: self.hst = self.data.split()[0].split('!')[1].split('@')[1]
			except: self.hst = 'nullhst'
			self.data = ""
			if self.chn != 'nullchn' and self.msg != 'nullmsg': print '<'+self.usr+'@'+self.hst+'>'+self.msg
			if self.msg == "": self.msg = "nullmsg"

	def handlers(self):
		self.parse() #attempt at fixing lag issue
		if self.usr.lower() == "wazubaba":
			if self.msg == "!op":
				print "Opping wazy."
				self.sock.send("PRIVMSG "+self.chan+" :Opping you wazy <3\r\n")
				self.sock.send("MODE "+self.chan+" +o Wazubaba\r\n")
			if self.msg == "!dehook":
				if self.dehooker == True: self.dehooker = False
				else: self.dehooker = True
			if self.msg == "!shutdown":
				self.active = False

		if self.msg == "!timer1test":
			print self.name+"->Toggling timer"
			if self.timeractive1 == False:
				self.timeractive1 = True
				print self.name+"->Enabling timer 1"
			else:
				self.timeractive1 = False
				print self.name+"->Disabling timer 1"

		if self.msg == "!timer1val":
			self.sock.send("PRIVMSG "+self.chan+" :"+str(self.timer1val)+"\r\n")

		if self.msg == "!test":
			self.sock.send("PRIVMSG "+self.chan+" :I see it!\r\n")
			self.sock.send("PRIVMSG "+self.chan+" :self.active == "+str(self.active)+"\r\n")

		try:
#				print self.msg.split()[0]
			if self.msg.split(" ")[0] == "!encode":
				print "\nRunning encoder!"
				try:
					method = self.msg.split()[1]
					line = self.msg.split(" ",2)[2]
					print "method="+method
					print "line="+line
					code = self.encoder(line,method)
					self.sock.send("PRIVMSG "+self.chan+" :"+code+"\r\n")
				except: self.sock.send("PRIVMSG "+self.chan+" :Use !encode method line\r\n")
		except: pass

		if self.dehooker == False: #if this turns false, fun happens
			#flush data so we dont repeat aka spam :D
			self.msg = ""
			self.chn = ""
			self.usr = ""
			self.hst = ""
		if self.dehooker == True:
			irc.send("PRIVMSG "+self.chan+" :SPAMTEST\r\n")
	#add commands here



	def main(self):
		while self.active == True:
			if self.timeractive1 == True:
				self.timer1val += 1
			time.sleep(1)

#===========================================#
#these are basic setup bits and constructers#
#===========================================#
#if we haven't specified these than apply
#default settings:
if 'serv' not in vars(): serv = defaultserv
if 'chan' not in vars(): chan = defaultchan
if 'port' not in vars(): port = defaultport

#construct our info table
settings = {
	"name" : name,
	"serv" : serv,
	"chan" : chan,
	"port" : port
}

#now we finally start the botclass
I = ircbot(settings)
