import threading
import socket
import sys
import time
import hashlib
import Queue as queue

#these are my core settings, configure as you wish:

name = "Youmu"
#defaultserv = "timeofeve.irc.so"
#defaultchan = "#timeofeve"
defaultserv = "irc.wazuclan.com"
defaultchan = "#wazuclan"
defaultport = 6667

#==================================================#

#this tests for arguments
if len(sys.argv)>1:# we always get 1 arg: the executed file
	try: chan=sys.argv[1]
	except:pass
	try: serv=sys.argv[2]
	except:pass
	try: port=int(sys.argv[3]) # we want port to be a number
	except:pass


class ircbot():
	def __init__(self,info={}):
		#deconstruct the table of data
		self.name = info["name"]
		self.serv = info["serv"]
		self.chan = info["chan"]
		self.port = info["port"]
		self.active = True
		self.data = ""
		self.lock = threading.Lock()
		self.dehooker = False
		self.connection = False

		self.msg = ""
		self.chn = ""
		self.usr = ""
		self.hst = ""

		self.kicktog = False

		self.testtimer1val = 0
		self.testtimer1status = False
		self.spammode = False
		self.target = ""
		self.connect()



		#start data grab thread
		threading.Thread(target = self.timerfunctions).start()

		#start main loop, where we handle cmds
		self.main()

	def connect(self):
		self.sock = socket.socket()
#		self.sock.settimeout(5)  # may be needed sometimes, idk
		self.sock.connect((self.serv,self.port))
		print self.name,"->Connecting to",self.serv,self.port
		self.sock.send('IDENT '+self.name+'\r\nNICK '+self.name+'\r\nUSER '+self.name+' '+self.name+' '+self.name+' '+' :'+self.name+'\r\n')
#		time.sleep(3)  # sometimes needed
		print self.name,"->Joining",self.chan
		self.sock.send('JOIN '+self.chan+'\r\n')
		print self.sock.recv(128)
		self.connection = True

	def main(self):
		#this is our main function, where we handle communication with the server
		while self.active:
			self.data = self.sock.recv(256)
			if self.data == "": self.connection = False
			while self.connection == False:
				try: self.connect()
				except: time.sleep(10) #wait for 10 seconds before trying to reconnect
			if self.data.find('KICK') != -1:
				if self.data.find(self.name) != -1:
					print self.name,"->Rejoining",self.chan
					self.sock.send('JOIN '+self.chan+'\r\n') 
					if self.kicktog:
						self.sock.send('PRIVMSG '+self.chan+' :Dont kick me! >_<\r\n')
					self.kicktog = True
			if self.data.find('PING') != -1: self.sock.send('PONG '+self.data.split()[1]+'\r\n')

			#parse the data, then do stuff based off it
			self.parse()
			self.handlers()



	def handlers(self):
		#This is where we check for commands and do things based off them
		if self.usr.lower() == "wazubaba":
			if self.msg == "!op":
				print "Opping wazy."
				self.sock.send("PRIVMSG "+self.chan+" :Opping you wazy <3\r\n")
				self.sock.send("MODE "+self.chan+" +o Wazubaba\r\n")
			if self.msg == "!dehook":
				if self.dehooker == True: self.dehooker = False
				else: self.dehooker = True
		

		if self.msg.split()[0] == "!shutdown":
			try:
				if self.msg.split()[1] == self.name:
					print "[L]"+self.usr+" issued shutdown."
					if self.usr.lower() == "wazubaba":
						self.active = False
					else:
						self.sock.send("PRIVMSG "+self.chan+" :Sorry, only Wazubaba can run that command :P\r\n")
			except: pass

		if self.msg == "!test":
			self.sock.send("PRIVMSG "+self.chan+" :I see it!\r\n")
			self.sock.send("PRIVMSG "+self.chan+" :self.active == "+str(self.active)+"\r\n")

		try:
#				print self.msg.split()[0]
			if self.msg.split(" ")[0] == "!encode":
				print "\nRunning encoder!"
				try:
					method = self.msg.split()[1]
					line = self.msg.split(" ",2)[2]
					print "method="+method
					print "line="+line
					code = self.encoder(line,method)
					self.sock.send("PRIVMSG "+self.chan+" :"+code+"\r\n")
				except: self.sock.send("PRIVMSG "+self.chan+" :Use !encode method line\r\n")
		except: pass

		if self.msg == "!tog1":
			if self.testtimer1status == False:
				self.testtimer1status = True
				self.sock.send("PRIVMSG "+self.chan+" :Enabling test timer :)\r\n")
			else:
				self.testtimer1status = False
				self.sock.send("PRIVMSG "+self.chan+" :Disabling test timer :)\r\n")

                if self.msg.split(" ")[0] == "!spam":
			print "[L]!spam called by "+self.usr
			if self.usr.lower() == "wazubaba":
				print "[L]!spam user accepted"
				try:
					self.target = self.msg.split(" ")[1]
					print "[L]Proper use of !spam confirmed"
					print "[L]Target: "+self.target
	        	                self.spammode = True
        	        	        self.sock.send("PRIVMSG "+self.chan+" :Enabling warmode :)\r\n")
				except:
					print "[L]Disabling warmode"
					self.spammode = False


		if self.msg == "!val1":
			self.sock.send("PRIVMSG "+self.chan+" :Test timer is currently at "+str(self.testtimer1val)+".\r\n")

		if self.msg == "!reset1":
			self.sock.send("PRIVMSG "+self.chan+" :Resetting timer 1 value :)\r\n")
			self.testtimer1val = 0

		if self.dehooker == False: #if this turns false, fun happens
			#flush data so we dont repeat aka spam :D
			self.msg = ""
			self.chn = ""
			self.usr = ""
			self.hst = ""
		if self.dehooker == True:
			irc.send("PRIVMSG "+self.chan+" :SPAMTEST\r\n")


	def timerfunctions(self):
		#this is where anything that needs to work ouside the main loop goes.
		while self.active:
			if self.testtimer1status == True:
				self.testtimer1val += 1

			if self.spammode == True:
				self.sock.send("PRIVMSG "+self.chan+" :"+self.target+"\r\n")
			time.sleep(1)


	def encoder(self,line,t):
		if t == "md5": m = hashlib.md5()
		elif t == "sha1": m = hashlib.sha1()
		elif t == "sha224": m = hashlib.sha224()
		elif t == "sha256": m = hashlib.sha256()
		elif t == "sha384": m = hashlib.sha384()
		elif t == "sha512": m = hashlib.sha512()
		m.update(line)
		gen = m.hexdigest()
		return gen

	def parse(self):
		# this ugly bit parses the data for the handler function
		if self.data.find('PRIVMSG'):
			try: self.msg = self.data.split(':',2)[2].strip('\r\n')
			except: self.msg = 'nullmsg'
			try: self.usr = self.data.split('!')[0][1:]
			except: self.usr = 'nullusr'
			try: self.chn = self.data.split()[2]
			except: self.chn = 'nullchn'
			try: self.hst = self.data.split()[0].split('!')[1].split('@')[1]
			except: self.hst = 'nullhst'
			self.data = ""
			if self.chn != 'nullchn' and self.msg != 'nullmsg': print '<'+self.usr+'@'+self.hst+'>'+self.msg
			if self.msg == "": self.msg = "nullmsg"




#===========================================#
#these are basic setup bits and constructers#
#===========================================#
#if we haven't specified these than apply
#default settings:
if 'serv' not in vars(): serv = defaultserv
if 'chan' not in vars(): chan = defaultchan
if 'port' not in vars(): port = defaultport

#construct our info table
settings = {
	"name" : name,
	"serv" : serv,
	"chan" : chan,
	"port" : port
}

#now we finally start the botclass
I = ircbot(settings)
