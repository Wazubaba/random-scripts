import socket
import sys
import os
import time


#BIG TODO: compile a list of hostnames for zero, ginger, orby, and me, then use that to auto op us based off usrname and host

if len(sys.argv)<2:
	print 'USAGE: <filename> channel network (port=6667 if left out)'
	sys.exit()

bn = 'Chacom'
chan = sys.argv[1]
net = sys.argv[2]
try:
	port = int(sys.argv[3])
except:
	port = 6667

adminlib = 'admins'

admins = {}

temp = open(adminlib,'r')
templn = temp.readlines()
temp.close()
for line in templn:
	admin, passw = line.split()
	admins[admin.lower()] = passw.strip('\n')
del templn, admin, passw

print bn,'creating socket...'
irc = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
print bn, 'connecting...'
irc.connect((net,port))
print bn, 'registering...'
irc.send('IDENT '+bn+'\r\n')
irc.send('NICK '+bn+'\r\n')
irc.send('USER '+bn+' '+bn+' '+bn+' '+' :'+bn+'\r\n')
time.sleep(3)
print bn, 'joining',chan+'...'
irc.send('JOIN '+chan+'\r\n')
print bn, 'ready.'

while 1:
	data = irc.recv(1024)
	if data.find('PING') != -1:
		irc.send('PONG '+data.split()[1]+'\r\n')
	elif data.find('PRIVMSG'):
		try:
			msg = data.split(':',2)[2].strip('\r\n')
		except:
			msg = 'nullmsg'
		try:
			usr = data.split('!')[0][1:]
		except:
			usr = 'nullusr'
		try:
			chn = data.split()[2]
		except:
			chn = 'nullchn'
		try:
			hst = data.split()[0].split('!')[1].split('@')[1]
		except:
			hst = 'nullhst'
		if chn != 'nullchn' and msg != 'nullmsg':
			print chn+' <'+usr+'@'+hst+'>'+msg
		try:
			splmsg = msg.split()
			try:
				'rawr' + splmsg[0]
			except:
				splmsg[0] = ''
		except:
			splmsg = ['nullmsg']
		if splmsg[0] == './op':
			try:
				if usr.lower() in admins:
					if admins[usr.lower()] == splmsg[1]:
						irc.send('MODE '+chan+' +o '+usr+'\r\n')
						irc.send('PRIVMSG '+chan+' :Opping '+usr+'\r\n')
			except:
				irc.send('PRIVMSG '+chan+' :please include the pass\r\n')
	else:
		print data

