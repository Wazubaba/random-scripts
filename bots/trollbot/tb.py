import socket
import sys
import os
import random
import time

if len(sys.argv)<2:
	chan = '#wazuclan'
	net = 'timeofeve.jp.pn'
try:
	if sys.argv[1] == '--help':
		print 'USAGE: <filename> channel network (port=6667 if left out)'
		sys.exit()
except: pass

bn = 'GingerBot'
try:
	chan = sys.argv[1]
	net = sys.argv[2]
except: pass

admin = [
	'wazubaba',
	'kitsune',
	'hokikomori',
	'snippy',
	'zeecaptain',
	]

help = [
	"./addtroll <msg> | Adds the message to my library. Put the <n> tag in it to represent the person's name",
	'./dowork <target> | trolls the target with a random line',
	'./numtroll  | Tells you how many toll messages I have',
	"./disclaimer | This is what keeps my writer's ass out of the fire",
	]

lib = 'trolls'
trollList = []
try:
	temp = open(lib,'r')
	ind = temp.readlines()
	for line in ind:
		trollList.append(line)
	temp.close()
	del temp
except:
	print 'Unable to load library, generating empty list'
	trollList = []
try:
	port = int(sys.argv[3])
except:
	port = 6667

print bn,'creating socket...'
irc = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
print bn, 'connecting...'
irc.connect((net,port))
print bn, 'registering...'
irc.send('IDENT '+bn+'\r\n')
irc.send('NICK '+bn+'\r\n')
irc.send('USER '+bn+' '+bn+' '+bn+' '+' :'+bn+'\r\n')
time.sleep(2)
print bn, 'joining',chan+'...'
irc.send('JOIN '+chan+'\r\n')
print bn, 'ready.'

while 1:
	data = irc.recv(1024)
	if data.find('PING') != -1:
		irc.send('PONG '+data.split()[1]+'\r\n')
	if data.find('PRIVMSG'):
		try:
			msg = data.split(':',2)[2].strip('\r\n')
		except:
			msg = 'nullmsg'
		try:
			usr = data.split('!')[0][1:]
		except:
			usr = 'nullusr'
		try:
			chn = data.split()[2]
		except:
			chn = 'nullchn'
		try:
			hst = data.split()[0].split('!')[1].split('@')[1]
		except:
			hst = 'nullhst'
		try:
			smsg = msg.split()
		except:
			smsg = ['']
		if chn != 'nullchn' and msg != 'nullmsg':
			print chn+' <'+usr+'@'+hst+'>'+msg

		if smsg[0] == './addtroll':
			try:
				trollList.append(msg.split(' ',1)[1])
				temp = open(lib,'w')
				print usr,'added',msg.split(' ',1)[1]
				for line in trollList:
					temp.write(line.strip('\n')+'\n')
				temp.close()
				del temp
			except: irc.send('PRIVMSG '+chan+' :Look'+usr+" if you can't even use the command right, why on earth should I write it down? O_o\r\n")
		if smsg[0] == './dowork':
			try:
				targ = msg.split(' ')[1]
				trollnum = random.randrange(0,len(trollList))
				trollout = trollList[trollnum].replace('<n>',targ)
				irc.send('PRIVMSG '+chan+' :'+trollout+'\r\n')
			except: irc.send('PRIVMSG '+chan+' :Hey everyone, '+usr+' just failed again!\r\n')
		if msg == './shutdown':
			if usr.lower() in admin:
				irc.send('QUIT\r\n')
				irc.close()
				time.sleep(2)
				sys.exit()
		if msg == './help':
			for line in help:
				irc.send('PRIVMSG '+chan+' :'+line+'\r\n')
		if msg == './disclaimer':
			irc.send('PRIVMSG '+chan+' :This bot is intended for abuse and general asshattery only, do not take this stuff seriously or you are an idiot, thank you ^_^\r\n')
		if msg == './numtroll':
			irc.send('PRIVMSG '+chan+' :I can troll you in '+str(len(trollList))+' ways.\r\n')
	else: print data

