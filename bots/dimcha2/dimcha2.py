import os
import sys
import datetime
import random
import time
import writeout
import imgcomp

__doc__='''
The DIMCHA bot, Desktop IMage CHAnger bot, is a bot to change the desktop
image in xfce.

This is the new mkII variant, that uses a standalone image list, to allow
easier changing of img libraries and customization.
<==========================================================================>
operational rundown: sleeps inbetween hours so as not to eat up unnessecary
cpu time, then ensures it has been an hour on the wake up cycle, and if so,
it will change BG, going back to sleep till next hour.

list of files included in the distribution:
	dimcha2.py  -> The main src file
	writeout.py -> handles datafile IO
	imgcomp.py  -> handles imglist IO

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

USAGE:
	python dimcha2.py arg [subarg] [subarg2]
	python dimcha2.py name-of-img-lib-to-load
	python dimcha2.py
		this last one will load the last library you opened
ARGS:
	--help -> display this help

	-comp  -> compile a new img library.
			USAGE: python dimcha2.py -comp newlib pathtoimgs

	-time  -> change the interval for desktop updates to the new setting,
		  time is referenced in minutes, so 60 = 1 hour.
			USAGE: python dimcha2.py -time 60

	-debug -> print out the current settings for time and library.
			USAGE: python dimcha2.py -debug

	-m -> designate which monitor dimcha should alter, 0 being the
		main monitor.
			USAGE: python dimcha2.py -m 0

	-s -> decide what style to use when setting the image.
		NOTE:
			0 - Auto
			1 - Centered
			2 - Tiled
			3 - Stretched
			4 - Scaled
			5 - Zoomed

			USAGE: python dimcha2.py -s 4

WARNINGS:
	The creator of this bot takes no responsibility for any potential
	damage it may cause.
	You use it at your own risk.

MISC:
	If for some reason a cmd doesn't work right, or a library
	is said to be corrupted, try executing dimcha from the src folder.

	Also, please note that when you run the -comp argument, if you are
	running dimcha via an alias, then whatever dir you are in will be
	where it looks for its img library. This means if it can't detect
	the current one, try to ensure you actually are in the right dir.

	I personally recommend just cd-ing to dimcha's dir, but if you want
	to have the files elsewhere, then whatevs.

'''

cp = writeout.datio()
try:
	cp.read('dat')
	sleeptime = int(cp.get('time'))
	lib = cp.get('hist')
	monitor = cp.get('monitor')
	style = cp.get('style')#
except:
	cp.dat['hist']=''
	cp.dat['time']='60'
	cp.dat['monitor']='0'
	cp.dat['style']='4'#
	cp.write('dat')
	sleeptime = 60

if len(sys.argv)>1:
	if '--help' in sys.argv:
		print __doc__
		sys.exit()

	elif sys.argv[1] == '-comp':
		try:
			targ = ''
			for line in sys.argv[3:]:
				targ += line
			libfile = sys.argv[2]
			imgs = imgcomp.imglist()
			print 'scanning',targ+'...'
			imgs.get(targ)
			print 'writing libfile',libfile+'...'
			imgs.write(libfile)
			print 'All done ^_^'
			sys.exit()
		except KeyError:
			print __doc__
			sys.exit()
		except OSError:
			print 'Error: Directory does not exist...'
			sys.exit()

	elif sys.argv[1] == '-time':
		time = sys.argv[2]
		cp.set('time',time)
		cp.write('dat')
		print 'Set time to ->',cp.get('time'),'minute(s).'
		sys.exit()

	elif sys.argv[1] == '-m':
		cp.set('monitor',sys.argv[2])
		cp.write('dat')
		print 'Set monitor to ->',cp.get('monitor')
		sys.exit()

	elif sys.argv[1] == '-s':#
		cp.set('style',sys.argv[2])#
		cp.write('dat')#
		print 'Set style to ->',cp.get('style')#
		sys.exit()#

	elif sys.argv[1] == '-debug':
		print cp.get('time')
		print cp.get('hist')
		print cp.get('monitor')
		print cp.get('style')#
		sys.exit()

	elif '-' in sys.argv:
		print __doc__
		sys.exit()
	else:
		lib = sys.argv[1]
		cp.set('hist',lib)
		cp.write('dat')

if cp.get('hist') == '':
	print 'No library has been specified, please compile a new library, or run -help for help'
	sys.exit()
try:
	imgs = imgcomp.imglist()
	imgs.read(lib)
except:
	if lib != -1:
		print 'Warning, library',lib,'is either corrupted or unreadable, please compile a new library or specify a different one.'
		sys.exit()
	else:
		print 'Warning, library not found!'
		sys.exit()

firstRun = True
print 'Dimcha is online ^_^.'

while 1:
	if not firstRun:
		print 'INFO:Dimcha is changing the background image.'
		choice = random.randrange(0,len(imgs.imglist))
		print 'INFO:Dimcha has selected',str(imgs.imglist[choice])
		os.system('xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitor'+monitor+'/image-path -s '+str(imgs.imgdir)+'/'+str(imgs.imglist[choice])+' 3')
		os.system('xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitor'+monitor+'/image-style -s '+style)
	else :
		firstRun = False
	toslp = int(cp.get('time'))*60
	print 'INFO:Dimcha is sleeping for', str(toslp), 'seconds.'
	time.sleep(toslp)
