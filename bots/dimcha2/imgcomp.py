#!/usr/bin/python
import os
class imglist():
	def __init__(self):
		self.imgform = [
                '.png',
                '.bmp', 
                '.jpg',
                '.jpeg',
                '.tga',
                '.gif',
                ] 
		self.imglist = []
		self.imgdir = ''

	def get(self,path):
		self.imgdir = path
		self.imgs = os.listdir(path)
		for img in self.imgs:
			for form in self.imgform:
				if form in img:
					self.imglist.append(img)
					break

	def write(self,file):
		temp = open(file,'w')
		temp.write(self.imgdir+'\n')
		for line in self.imglist:
			temp.write(line+'\n')
		temp.close()

	def read(self,file):
		temp = open(file,'r')
		tl = temp.readlines()
		temp.close()
		self.imgdir = tl[0].strip('\r\n')
		for line in range(1,len(tl)):
			if tl[line] != '' or '\n' or ' ' or '\r\n':
				self.imglist.append(tl[line].strip('\r\n'))
