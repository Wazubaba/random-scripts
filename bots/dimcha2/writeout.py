class datio():
	def __init__(self):
		self.dat = {}
	def read(self,file):
		temp = open(file,'r')
		tl = temp.readlines()
		temp.close()
		for line in tl:
			temp = line.split('=')
			self.dat[temp[0]] = temp[1].strip('\n')
	def write(self,file):
		temp = open(file,'w')
		for key in self.dat:
			temp.write(key+'='+str(self.dat[key])+'\n')
		temp.close()
	def set(self,key,val):
		self.dat[key] = val

	def get(self,key):
		return self.dat[key]

