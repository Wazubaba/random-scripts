#! /usr/bin/python
import socket,sys,time,random

print '     -=[ Cofli ]=-'
print '   -=[By Wazubaba]=-'
print ' -=[timeofeve.jp.pn]=-'

if len(sys.argv)<2:
	print 'USAGE: <filename> channel network (port=6667 if left out)'
	sys.exit()

bn = 'Cofli'
chan = sys.argv[1]
net = sys.argv[2]
chanlist = []

prefix = '!'
help = '''
	%sjoin - joins a channel
	%spart - leaves a channel
	%shelp - displays this help
''' %(prefix,prefix,prefix)

try: port = int(sys.argv[3])
except: port = 6667

chanlist.append(chan)
print bn,'creating socket...'

irc = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
print bn, 'connecting...'

irc.connect((net,port))
print bn, 'registering...'

irc.send('IDENT '+bn+'\r\nNICK '+bn+'\r\nUSER '+bn+' '+bn+' '+bn+' '+' :'+bn+'\r\n')

time.sleep(3)
print bn, 'joining',chan+'...'

irc.send('JOIN '+chan+'\r\n')
print bn, 'ready.'


while 1:
	data = irc.recv(1024)
	if data.find('PING') != -1: irc.send('PONG '+data.split()[1]+'\r\n')
	if data.find('PRIVMSG'):
		try: msg = data.split(':',2)[2].strip('\r\n')
		except: msg = 'nullmsg'
		try: usr = data.split('!')[0][1:]
		except: usr = 'nullusr'
		try: chn = data.split()[2]
		except: chn = 'nullchn'
		try: hst = data.split()[0].split('!')[1].split('@')[1]
		except: hst = 'nullhst'
  
		try:
			splmsg = msg.split()
			try:
				if splmsg[0] != 0:
					pass
			except:
				splmsg[0] = ' '
		except: splmsg = ['nullmsg']
  
		if chn != 'nullchn' and msg != 'nullmsg': print chn+' <'+usr+'@'+hst+'>'+msg
  
		if msg == '': continue
  
		if msg == prefix+'help':
			irc.send('PRIVMSG '+usr+' :'+help+'\r\n')
   
		if splmsg[0] == prefix+'join':
			try:
				chanlist.append(splmsg[1])
				irc.send('JOIN '+splmsg[1]+'\r\n')
			except: irc.send('NOTICE '+usr+' :Please specify a channel to join.\r\n')

		if splmsg[0] == prefix+'part':
			try:
				if splmsg[1] in chanlist:
					irc.send('PART '+splmsg[1]+'\r\n')
			except: irc.send('NOTICE '+usr+' :Please specify a channel to part.\r\n')
   
		if splmsg[0] == prefix+'wat':
			try:
				if msg.split(' ',1)[1] == 'onefish,twofish,redfish':
					irc.send('PRIVMSG '+chn+' :bluefish\r\n')
					continue
				stuffs = msg.split(' ',1)[1]
				stuffs = stuffs.split(',')
				irc.send('PRIVMSG '+chn+' :'+random.choice(stuffs)+"\r\n")
			except: pass

		if splmsg[0] == prefix+'say':
			try: irc.send('PRIVMSG '+chan+' :'+msg.split(' ',1)[1]+'\r\n')
			except: pass
	else: print data
