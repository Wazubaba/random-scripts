import socket
import sys
import os
import time

if len(sys.argv)<2:
	print 'USAGE: <filename> channel network (port=6667 if left out)'
	sys.exit()

bn = 'Calc'
chan = sys.argv[1]
net = sys.argv[2]
try:
	port = int(sys.argv[3])
except:
	port = 6667

print bn,'creating socket...'
irc = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
print bn, 'connecting...'
irc.connect((net,port))
print bn, 'registering...'
irc.send('IDENT '+bn+'\r\n')
irc.send('NICK '+bn+'\r\n')
irc.send('USER '+bn+' '+bn+' '+bn+' '+' :'+bn+'\r\n')
time.sleep(3)
print bn, 'joining',chan+'...'
irc.send('JOIN '+chan+'\r\n')
print bn, 'ready.'

while 1:
	data = irc.recv(1024)
	if data.find('PING') != -1:
		irc.send('PONG '+data.split()[1]+'\r\n')
	if data.find('PRIVMSG'):
		try:
			msg = data.split(':',2)[2].strip('\r\n')
		except:
			msg = 'nullmsg'
		try:
			usr = data.split('!')[0][1:]
		except:
			usr = 'nullusr'
		try:
			chn = data.split()[2]
		except:
			chn = 'nullchn'
		try:
			hst = data.split()[0].split('!')[1].split('@')[1]
		except:
			hst = 'nullhst'
		try:
			splmsg = msg.split()
		except:
			splmsg = [msg]
		if chn != 'nullchn' and msg != 'nullmsg':
			print chn+' <'+usr+'@'+hst+'>'+msg
		if splmsg[0] == './eval':
			try:
				irc.send('PRIVMSG '+usr+' :'+str(eval(msg.strip('./eval ').strip('\r\n')))+'\r\n')
				print usr+'->',msg.strip('./eval').strip('\r\n'),'\nsolution->',eval(msg.strip('./eval ').strip('\r\n'))
			except ZeroDivisionError:
				irc.send('PRIVMSG '+usr+' :While I fully agree that dividing by zero is possible, I do not with to think of the answer to this, so go awai\r\n')
			except:
				try:
					print eval(msg)
				except:
					pass
				irc.send('PRIVMSG '+usr+' :Unable to solve\r\n')
		if msg == './calc help':
			help = [
				'./eval -> will attempt to solve the equation following',
				'syntax is as follows:',
				'+=addition -=subtraction /=division *=multiplication ()=parenthesis ^= modulo **=exponent',
			]
			for line in help:
				irc.send('PRIVMSG '+usr+' :'+line+'\r\n')
	else:
		print data

