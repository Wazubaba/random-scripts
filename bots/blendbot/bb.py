import socket
import sys
import os
import time
import random

if len(sys.argv)<2:
	print 'USAGE: <filename> channel network (port=6667 if left out)'
	sys.exit()

bn = 'BlendTec-blendbot'
chan = sys.argv[1]
net = sys.argv[2]
try:
	port = int(sys.argv[3])
except:
	port = 6667

print bn,'creating socket...'
irc = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
print bn, 'connecting...'
irc.connect((net,port))
print bn, 'registering...'
irc.send('IDENT '+bn+'\r\n')
irc.send('NICK '+bn+'\r\n')
irc.send('USER '+bn+' '+bn+' '+bn+' '+' :'+bn+'\r\n')
time.sleep(3)
print bn, 'joining',chan+'...'
irc.send('JOIN '+chan+'\r\n')
print bn, 'ready.'

while 1:
	data = irc.recv(1024)
	if data.find('PING') != -1:
		irc.send('PONG '+data.split()[1]+'\r\n')
	if data.find('PRIVMSG'):
		try:
			msg = data.split(':',2)[2].strip('\r\n')
		except:
			msg = 'nullmsg'
		try:
			usr = data.split('!')[0][1:]
		except:
			usr = 'nullusr'
		try:
			chn = data.split()[2]
		except:
			chn = 'nullchn'
		try:
			hst = data.split()[0].split('!')[1].split('@')[1]
		except:
			hst = 'nullhst'
		if chn != 'nullchn' and msg != 'nullmsg':
			print chn+' <'+usr+'@'+hst+'>'+msg
		try: splmsg = msg.split()
		except: splmsg = ['nullmsg']

		if splmsg[0] == './willitblend':
			try:
				things = msg.split(' ',1)[1]
			except: things = ["air"]
			try: things = things.split(',')
			except: pass
			try:
				 if things[1] == '': things=things[0]
			except: pass
			irc.send('PRIVMSG '+chan+' :Test results:\r\n')
			if len(things)>5:
				irc.send('PRIVMSG '+chan+" :The blender can't take that many things at once, please reduce the amount of things to test.\r\n")
				continue
			for thing in things:
				try:
					if thing[0] == ' ':thing=thing[1:]
				except: pass
				irc.send('PRIVMSG '+chan+' :'+thing+'> '+random.choice(['blends', "doesn't blend"])+"\r\n")
				
	else:
		print data

