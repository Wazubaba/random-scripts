#! /usr/bin/python
import socket,sys,time

print '-=[ Irc bot template ]=-'
print '   -=[By Wazubaba]=-'
print ' -=[timeofeve.jp.pn]=-'

#This is what checks to make sure there are enough arguments
#to tell us what server and channel to connect to
if len(sys.argv)<2:
	print 'USAGE: <filename> channel network (port=6667 if left out)'
	sys.exit() #If we don't have enough, display a help message and quit

bn = 'BotFrameworkMKIII' #This is the name the bot will be connected as
chan = sys.argv[1] #Assign the cmdline arg to the chan
net = sys.argv[2]  #Assign the cmdline arg to the server
chanlist = [] #This is removable if needed, it is for allowing connection to several channels at once.

prefix = '/'
#This is the prefix, you don't need it,
#but if you want to display the help text properly
#without having to manually add the symbol each time,
#just change this to whatever you use ^_^

help = '''
	%sjoin - joins a channel
	%spart - leaves a channel
	%shelp - displays this help
''' %(prefix,prefix,prefix)
#Five gueses what this is :P

#This allows a specified port to also be used, in case of non standard ports on a serv
try: port = int(sys.argv[3])
except: port = 6667

#This is what actually makes the connection to the server

#Append the first chan to our list
chanlist.append(chan)
print bn,'creating socket...'

#Make a socket to the server
irc = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
print bn, 'connecting...'

#Actually connect to the server
irc.connect((net,port))
print bn, 'registering...'

#Send some data to the server, so it knows who we are
irc.send('IDENT '+bn+'\r\nNICK '+bn+'\r\nUSER '+bn+' '+bn+' '+bn+' '+' :'+bn+'\r\n')

#This line is optional, but in my experience, you sometimes need it
#and sometimes need to adjust it if your bot just won't connect.
#Recommended range is 2-5
time.sleep(3)
print bn, 'joining',chan+'...'

#Actually join the channel.
irc.send('JOIN '+chan+'\r\n')
print bn, 'ready.'


#Our main loop, this handles data collection, keep alive, and commands.
while 1:
	#Store the data for parsing
	data = irc.recv(1024)
 	
	#Keep alive, so we stay connected
	if data.find('PING') != -1: irc.send('PONG '+data.split()[1]+'\r\n')
 
	#If the data contains a message, parse it for certain data
	if data.find('PRIVMSG'):
		try: msg = data.split(':',2)[2].strip('\r\n')
		except: msg = 'nullmsg'
		try: usr = data.split('!')[0][1:]
		except: usr = 'nullusr'
		try: chn = data.split()[2]
		except: chn = 'nullchn'
		try: hst = data.split()[0].split('!')[1].split('@')[1]
		except: hst = 'nullhst'
  
		#This creates a split up version of the message, split by spaces
		try:
			splmsg = msg.split()
			try:
				if splmsg[0] != 0:
					pass
			except:
				splmsg[0] = ' '
		except: splmsg = ['nullmsg']
  
		#This ensures that we don't get a random null message due to server keep alive
		if chn != 'nullchn' and msg != 'nullmsg': print chn+' <'+usr+'@'+hst+'>'+msg
  
		#This makes certain that we don't try to parse an empty message
		if msg == '': continue
  
		#This is a sample help command
		if msg == prefix+'help':
			irc.send('PRIVMSG '+usr+' :'+help+'\r\n')
   
		#This is a sample join command
		if splmsg[0] == prefix+'join':
			try:
				chanlist.append(splmsg[1])
				irc.send('JOIN '+splmsg[1]+'\r\n')
			except: irc.send('NOTICE '+usr+' :Please specify a channel to join.\r\n')

		#This is a sample part command
		if splmsg[0] == prefix+'part':
			try:
				if splmsg[1] in chanlist:
					irc.send('PART '+splmsg[1]+'\r\n')
			except: irc.send('NOTICE '+usr+' :Please specify a channel to part.\r\n')
   
	#This is a solve all way to ensure we get all the data, in case we just can't parse it.
	else: print data
