import socket
import time
import threading

class bot:
	def __init__(self, name):
		self.name = name
		self.chans = []

	def connect(self,net,port = 6667):
		print 'creating socket...'
		self.irc = socket.socket()
		print 'connecting...'
		self.irc.connect((net,port))
		print 'registering...'
		self.irc.send('IDENT '+self.name+'\r\n')
		self.irc.send('NICK '+self.name+'\r\n')
		self.irc.send('USER '+self.name+' '+self.name+' '+self.name+' :'+self.name+'\r\n')
		print 'connection established.'
		self.maintainer = threading.Thread(target = self.main)
		self.maintainer.start()

	def join(self,chan):
		self.chans.append(chan)
		self.irc.send('JOIN '+chan+'\r\n')

	def part(self,chan):
		self.chans = self.chans.remove(chan)
		if self.chans == None: self.chans = []
		self.irc.send('PART '+chan+'\r\n')

	def sendmsg(self,chan,msg):
		self.irc.send('PRIVMSG '+chan+' :'+msg+'\r\n')

	def funcs(self):
		if self.msg == '\test':
			self.sendmsg('test works! ^_^')
		return

	def main(self):
		while 1:
			self.data = self.irc.recv(1024)
			if self.data.find('PING') != -1:
				self.irc.send('PONG '+self.data.split()[1]+'\r\n')
			if self.data.find('PRIVMSG'):
				try:
					self.msg = self.data.split(':',2)[2].strip('\r\n')
				except:
					self.msg = 'nullmsg'
				try:
					self.usr = self.data.split('!')[0][1:]
				except:
					self.usr = 'nullusr'
				try:
					self.chn = self.data.split()[2]
				except:
					self.chn = 'nullchn'
				try:
					self.hst = self.data.split()[0].split('!')[1].split('@')[1]
				except:
					self.hst = 'nullhst'
				if self.chn != 'nullchn' and self.msg != 'nullmsg':
					print self.chn+' <'+self.usr+'@'+self.hst+'>'+self.msg
				self.funcs()
			else:
				print self.data
rawr = bot('rawrmod')
rawr.connect('localhost')

rawr.join('#wazuhome')
#while 1:
#	print rawr.msg
#	time.sleep(0.2)
