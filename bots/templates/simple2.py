import socket,sys,time

if len(sys.argv)<2:
	print 'USAGE: <filename> channel network (port=6667 if left out)'
	sys.exit()

bn = 'Bot'
chan = sys.argv[1]
net = sys.argv[2]
try: port = int(sys.argv[3])
except: port = 6667

print bn,'creating socket...'
irc = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
print bn, 'connecting...'
irc.connect((net,port))
print bn, 'registering...'
irc.send('IDENT '+bn+'\r\nNICK '+bn+'\r\nUSER '+bn+' '+bn+' '+bn+' '+' :'+bn+'\r\n')
time.sleep(3)
print bn, 'joining',chan+'...'
irc.send('JOIN '+chan+'\r\n')
print bn, 'ready.'

while 1:
	data = irc.recv(1024)
	if data.find('PING') != -1: irc.send('PONG '+data.split()[1]+'\r\n')
	if data.find('PRIVMSG'):
		try: msg = data.split(':',2)[2].strip('\r\n')
		except: msg = 'nullmsg'
		try: usr = data.split('!')[0][1:]
		except: usr = 'nullusr'
		try: chn = data.split()[2]
		except: chn = 'nullchn'
		try: hst = data.split()[0].split('!')[1].split('@')[1]
		except: hst = 'nullhst'
		try: splmsg = msg.split()
		except: splmsg = ['nullmsg']
		if chn != 'nullchn' and msg != 'nullmsg': print chn+' <'+usr+'@'+hst+'>'+msg
		if msg == '': continue
		
	else: print data
