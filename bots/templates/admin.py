# read(file to load) returns( dict of admins)
# write(file to load, data to write(needs to be a dict), debug=False(for printing info)
# append(data(needs to be a dict),Adminname and pass(must be in a tuple), debug=False(for printing info) returns(dict of admins)


def read(lib,debug=False):
	try:
		temp = open(lib,'r')
	except IOError:
		print lib,'does not exist, creating empty library...'
		temp = open(lib,'w')
		temp.close()
		return []
	dat = temp.readlines()
	temp.close()
	admins = {}
	for line in dat:
		temp = line.split('|')
		admins[temp[0]] = temp[1].strip('\n')
	if debug:print 'Loaded admins table from',lib
	return admins

def write(lib,data,debug=False):
	temp = open(lib,'w')
	for line in data:
		temp.write(line.lower()+'|'+data[line]+'\n')
	temp.close()
	if debug:print 'Saved data to',lib
	return
			

def append(data,adminPass,debug=False):
	if type(adminPass) != tuple or type(data) != dict: raise Exception('append() requires the admin library to be a dictionary and the new key/data values to be in a tuple.')
	if debug:
		print 'Adding',adminPass[0],'with password',adminPass[1],'to the admin library...'
	newdat = data
	newdat[adminPass[0].lower()] = adminPass[1]
	return newdat

def TESTMODE():
	temp = read('test')
	temp = append(temp,('test','2'))
	write('test',temp)
	temp = append(temp,('rawr','mod'))
	write('test',temp)
	print read('test')
