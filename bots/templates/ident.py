#ident(username,password,adminlist(dictionary),identlist(list),debug=False),returns(identlist(list))
#logout(identlist(list),username,debug=False),returns(identlist(list))
#isidented(username,identlist(list),debug=False),returns True or False

def ident(usr,passw,adminlist,identlist,debug=False):
	if usr.lower() in adminlist:
		if adminlist[usr.lower()] == passw:
			if debug:print 'User',usr,'has identified.'
			identlist.append(usr.lower())
			return identlist
	return False

def logout(identlist,usr,debug=False):
	if usr.lower() in identlist:
		return identlist.remove(usr.lower())

def isidented(usrname,identlist,debug=False):
	if usrname.lower() in identlist:
		if debug:print usrname.lower(),'is idented.'
		return True
	else:
		return False


########################################################
# Example useage #######################################
########################################################	
#admins = {'rawrmod':'999'}
#idented = []
#attempt = ident('rawrmod','999',admins,idented,True)
#if attempt != False:idented = attempt
