#!/usr/bin/python
#######################################################################
#           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE               #
#                   Version 2, December 2004                          #
#                                                                     #
#	  Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>            #
#                                                                     #
# Everyone is permitted to copy and distribute verbatim or modified   #
# copies of this license document, and changing it is allowed as long #
# as the name is changed.                                             #
#                                                                     #
#           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE               #
#  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION    #
#                                                                     #
# 0. You just DO WHAT THE FUCK YOU WANT TO.                           #
#######################################################################
#as a note, I was on a kick programming C style, so my print calls have random ()'s, and my lines
#are mostly terminated with ;'s. This shouldn't cause any problems, and is entirely optional.
#I'll probably strip them out later anyways :P


import socket,sys,time

bn = "template4"; #this is the bot's name

#this tests for arguments, it could easilly be altered to let you manually input them with raw_input() :D
if len(sys.argv) < 3: exit("Usage: "+sys.argv[0]+" channel(no first # needed) server <optional port>"); 

print("[I]%s loading..." %bn);


#this is our bot class. You don't need to use a class for one like this, but if you do it can be a lot more organized.
class ircbot():
	#as a note to those who don't know, doing the "%s" blah is the same as doing "stuff "+var+" here"
	def __init__(self, name): #this configures everything up for us
		self.chan = "#%s" %(sys.argv[1]); #this makes it so we don't have to add an initial # to our channel on commandline
		self.serv = sys.argv[2];
		self.bn = name;

		if len(sys.argv) > 3: self.port = int(sys.argv[4]);
		else: self.port = 6667;

		print("%s\n%s" %(self.chan, self.serv));
		self.irc = socket.socket();

		#vars we will use later
		self.msg = self.chn = self.usr = self.hst = ""; #set everything to an empty string
		self.active = 0;

		self.connect();

	def connect(self): #this connects us to the server
		print("[I]%s opening socket." %self.bn);
		self.irc.connect((self.serv,self.port));
		print("[I]%s connected to %s, identifying..." %(self.bn, self.serv));
		self.irc.send("IDENT %s\r\nNick %s\r\nUSER %s %s %s L%s\r\n" %(self.bn,self.bn,self.bn,self.bn,self.bn,self.bn)); #TODO: find a way to not repeat the same var 6 times -_-
		time.sleep(3); #this is sometimes needed, sometimes not needed.
		#If you can comment it out and still connect, by all means do, just remember that if on a certain day it refuses to join the channel correctly, try uncommenting this.
		print("[i]%s has identified, now joining channel %s" %(self.bn, self.chan));
		self.irc.send("JOIN %s\r\n" %self.chan);
		print(self.irc.recv(128));
		self.active = 1;#this is saying that the bot is online and ready to go
		self.status = 1;#this is what says we are supposedly connected and on a channel
		self.main();

	def format(self): #this splits everything up so we can just check a few vars for info
                if self.data.find('PRIVMSG'):
                        try: self.msg = self.data.split(':',2)[2].strip('\r\n')
                        except: self.msg = 'nullmsg'
                        try: self.usr = self.data.split('!')[0][1:]
                        except: self.usr = 'nullusr'
                        try: self.chn = self.data.split()[2]
                        except: self.chn = 'nullchn'
                        try: self.hst = self.data.split()[0].split('!')[1].split('@')[1]
                        except: self.hst = 'nullhst'
                        self.data = ""
                        if self.chn != 'nullchn' and self.msg != 'nullmsg': print '<'+self.usr+'@'+self.hst+'>'+self.msg
                        if self.msg == "": self.msg = "nullmsg"
			#basically, all of this formats the raw data into an easilly workable form, then prints a standard
			#line out for our terminals/logs (assuming you start piping the bot to a file)
			#as a note, for some reason the host name randomly fails to be aquired.



	def main(self): #this is where we do stuffs, like test for cmds ^_^
		while self.active:
			self.data = self.irc.recv(256);
			if self.data == "": self.status = 0
			while self.status == 0: #this tests if the bot is even connected to the network, if it gets booted, this will try to reconnect.
				try: self.connect();
				except: time.sleep(10); #wait 10 seconds if we can't reconnect before trying again
			if self.data.find("KICK") != -1: #this tests if the bot was kicked, and if so rejoins.
				if self.data.find(self.bn) != -1:
					print("[I]%s has been kicked from %s, rejoining..." %(self.bn, self.chan));
					self.irc.send("JOIN %s\r\n" %self.chan);
			if self.data.find("PING") != -1:
				self.irc.send("PONG %s\r\n" %self.data.split()[1]); #this is the keepalive, think of it as the bot's heartbeat

			if self.data.find("!shutdown") != -1: self.active = 0; #this is a really basic shutdown command. Be warned, if anyone types !shutdown anywhere in a sentance with this
			#method, the bot will shutdown, better method would be something like this:
			#if self.usr == "yournamehere":
			#	if self.msg == "!shutdown":
			#		self.active = 0

			self.format();
			#------------#
			#new commands#
			# go   there #
			#VVVVVVVVVVVV#

bot = ircbot(bn); #sets our bot up
