#load(filename,debug=False),returns(list)
#save(filename,data(list),debug=False)

def load(filename,debug=False):
	try:
		temp = open(filename,'r')
	except IOError:
		print 'File does not exist. Returning blank list...'
		return []
	dat = temp.readlines()
	temp.close()
	temp = []
	for line in dat:
		temp.append(line.strip('\n'))
	if debug:print filename,'loaded successfully!'
	return temp

def save(filename,data,debug=False):
	temp = open(filename,'w')
	for line in data:
		temp.write(line.lower()+'\n')
	temp.close()
	if debug:print filename,'saved successfully!'
	return
